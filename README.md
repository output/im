# 说明

使用环境:
PHP>=7.1
Swoole >= 4.3.0
Redis、PDO
Composer 更新需要用

系统推送消息给用户，
 用户之间相互聊天，
 群组消息



这是一个 WebSocket、Http 共存服务的示例
127.0.0.1改成你服务器的地址

WebSocket:`ws://127.0.0.1:8081/ws`

Http:<http://127.0.0.1:8081/>、<http://127.0.0.1:8081/api>

`test-html/index.html` 文件可以连接 WebSocket 进行调试

## 安装

### 方法

* git 拉取下本项目或者下载本包后 执行命令

* 在本项目目录中，执行命令：`composer update`


## 启动命令

在本项目目录中，执行命令：`vendor/bin/imi server/start`
（需要php版本不要禁止exec等命令启动时候报错请查看是否禁用了某函数）

停止命令 `vendor/bin/imi server/stop`

## 权限

`.runtime` 目录需要有可写权限


此源码基于imi 框架
